package br.com.PlatformBuilders.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.PlatformBuilders.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
	public Client findByCpf(String cpf);
    public List<Client> findByNomeIgnoreCaseContaining(String nome);

}
