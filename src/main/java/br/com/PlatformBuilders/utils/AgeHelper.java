package br.com.PlatformBuilders.utils;

import java.time.LocalDate;
import java.time.Period;

import br.com.PlatformBuilders.model.Client;

public class AgeHelper {

	public static int findAge(Client client) {

		return (int) Period.between(client.getDataNascimento(), LocalDate.now()).getYears();
	}
}
