package br.com.PlatformBuilders.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.PlatformBuilders.dto.ClientDTO;
import br.com.PlatformBuilders.errors.ResourceNotFoundException;
import br.com.PlatformBuilders.model.Client;
import br.com.PlatformBuilders.service.ClientService;

@RestController
@RequestMapping(path = "/v1/clients")
public class ClientController {

	private ClientService service;

	@Autowired
	public ClientController(ClientService service) {
		this.service = service;
	}

	@GetMapping
	public ResponseEntity<?> listAll(@PageableDefault(page = 0, size = 5, sort = "id") Pageable paginacao) {

		Page<Client> clientes = service.findAll(paginacao);
		return new ResponseEntity<>(clientes, HttpStatus.OK);
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<?> findClientById(@PathVariable("id") Long id) {
		verifyIfClientExists(id);
		Optional<Client> client = service.findById(id);
		return new ResponseEntity<>(client.get(), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<?> saveClient(@Valid @RequestBody Client Client) {

		Client ClientCreate = service.save(Client);
		return new ResponseEntity<>(ClientCreate, HttpStatus.CREATED);
	}

	@PutMapping(path = "/{id}")
	public ResponseEntity<?> updateClient(@PathVariable Long id, @RequestBody Client ClientDto) {
		verifyIfClientExists(id);
		Client newClient = service.save(ClientDto);

		return ResponseEntity.ok(newClient);
	}

	@PatchMapping(path = "/{id}")
	public ResponseEntity<?> changeClient(@RequestBody ClientDTO ClientDTO, @PathVariable("id") Long id) {
		verifyIfClientExists(id);
		Client client = service.getOne(id);
		client.setCpf(ClientDTO.getCpf());
		client.setNome(ClientDTO.getNome());
		client.setDataNascimento(ClientDTO.getDataNascimento());
		service.save(client);
		return new ResponseEntity<>(client, HttpStatus.OK);
	}

	@GetMapping(path = "/findByName/{nome}")
	public ResponseEntity<?> findClientByNameIgnoreCaseContaining(@RequestParam String nome) {

		List<Client> clientList = service.findByNomeIgnoreCaseContaining(nome);
		return new ResponseEntity<>(clientList, HttpStatus.OK);
	}

	@GetMapping(path = "/findByCpf/{cpf}")
	public ResponseEntity<?> findByCpf(@RequestParam String cpf) {
		Client client = service.findByCpf(cpf);
		return new ResponseEntity<>(client, HttpStatus.OK);

	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<?> deleteClient(@PathVariable("id") Long id) {
		verifyIfClientExists(id);
		service.deleteById(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	private void verifyIfClientExists(Long id) {
		Optional<Client> clientExists = service.findById(id);
		if (!clientExists.isPresent())
			throw new ResourceNotFoundException("Client not found");
	}
}
