package br.com.PlatformBuilders.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.PlatformBuilders.model.Client;
import br.com.PlatformBuilders.repository.ClientRepository;

@Service
public class ClientService {

	private ClientRepository repository;

	@Autowired
	public ClientService(ClientRepository repository) {
		this.repository = repository;
	}

	public Page<Client> findAll(Pageable pages) {
		Page<Client> page = repository.findAll(pages);
		return page;
	}

	public Optional<Client> findById(Long id) {
		Optional<Client> optionalClient = repository.findById(id);
		return optionalClient;
	}

	public Client save(Client client) {
		return repository.save(client);
	}

	public List<Client> findByNomeIgnoreCaseContaining(String nome) {
		List<Client> clientList = repository.findByNomeIgnoreCaseContaining(nome);
		return clientList;
	}

	public Client findByCpf(String cpf) {
		Client client = repository.findByCpf(cpf);
		return client;
	}

	public void deleteById(Long id) {
		repository.deleteById(id);
	}

	public Client getOne(Long id) {
		Client client = repository.getOne(id);
		return client;
	}
}
